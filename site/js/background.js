/* 
diy snow flakes the for background
can change : 
-speed(mspeed)
-size(rad)
-distance apart(dist)
-number of them(numFlakes)
-and where they start(x,y)
-max and min opaticy
-color as well!
*/

var numFlakes = 150; 
var mSpeed = 4; 
var minO = .6;
var maxO = 1;
var radSize = 4;
var r = 255;
var g = 255;
var b = 255;

function setColor(red,green,blue){
    this.r = red;
    this.g = green;
    this.b = blue;
} 

function setSpeed(num){
    mSpeed = num;
}

function setRadSize(num){
    radSize = num;
    console.log(radSize);
}

function makeFlakes(){

	var canvas = document.getElementById("canvas");
	var ctx = canvas.getContext("2d");
	
	//canvas dimensions
	var width = window.innerWidth;
	var height = window.innerHeight;
	
    canvas.width = width;
	canvas.height = height;
    var holder;
 
    
    //constructor for the snow flakes it takes position of x,y, size of the snow, and dist from each flake
    function snowFlake(x,y,rad,dist){
        this.x = x; 
        this.y = y;
        this.rad = rad; //size of the flakes
        this.dist = dist; //distance from the flakes
        this.speed = Math.random(); 
        this.o = Math.random () * (maxO - minO) + minO; //opaticy of the flakes 
    }
    
	//starts making the snowflakes and place and get them into a array 
	var flakes = [];
	for(var i = 0; i < numFlakes; i++){
        holder = new snowFlake(Math.random()*width,Math.random()*height,Math.random()*radSize+1,Math.random()*numFlakes);
        flakes.push(holder);
        
	}
    //console.log(flakes); for debug 
	
    
	//the draw function for looping
	function draw() {
        //clears the canvas
		ctx.clearRect(0, 0, width, height);

		
		for(var i = 0; i < numFlakes; i++){
            ctx.beginPath();
			var snow = flakes[i]; //holder
			ctx.moveTo(snow.x, snow.y);
			ctx.arc(snow.x, snow.y, snow.rad, 0, Math.PI*2, true);//makes the circles
            ctx.fillStyle = "rgba("+r+','+g+','+b+','+snow.o+")"; //color of flakes
		    ctx.fill();
		}

		update();
	}
	
	//Function to move the snowflakes, updates per amount  of ms
	
	function update() {
		
		for(var i = 0; i < numFlakes; i++) {
			var snow = flakes[i]; //holder
            
            snow.y += snow.speed * mSpeed + 1; //speed of the flakes
			
			//make new flakes after one exits
			if(snow.x > width + snow.rad || snow.y > height - snow.rad ){
                    flakes[i] = new snowFlake(Math.random()*width,0,Math.random()*radSize+1,Math.random()*numFlakes);
                    
            } 
	
        }
    }

	//Looping and redrawing
	setInterval(draw, 60);
 }