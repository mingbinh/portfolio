var name = "Name: Mingbin Huang";
var phoneNumber = "Work Phone: 206-859-9014";
var email = "<a href='mailto:mingbinh@uw.edu'>Email: mingbinh@uw.edu</a>";
var timer;
var timer2;
var contactInfo = [name,phoneNumber,email];
var index = 0;//for list
var index_2 = 0 ; //for snowMan 



//add every item of the list
//to the ul and it stop
function contactList(){
        
    $('ul').append("<li class = 'list-group-item'>" + contactInfo[index]+"</li>");
    index++;
    if(index == contactInfo.length){
        clearInterval(timer);
    }
}




/* DIY snow man for fun and decorations and a way to say thank you*/


//x,y,rad,0,2*pI (circle)
//higher y lower it is
//higher x more right
function snowMan(){
    var canvas = document.getElementById("canvas_2");
    var ctx = canvas.getContext('2d');
    var height = (canvas.height/1.3); //cutting down on the box
    var width = canvas.width;
    var size = 1.2;//size of the ball
    
    //pervious values obtained by trial and error(goes by shadow,ball)
    var sHead = new snowBall(31,59,15,"lightgray");
    var head = new snowBall(34,60,15,"white");
    var sMiddle = new snowBall(32,28,20,"lightgray");
    var middle = new snowBall(35,30,20,"white");
    var sLow = new snowBall(33,-5,25,"lightgray");
    var low = new snowBall(36,0,25,"white");
    
    var leftEye = new snowBall(28,63,1.8,"black");
    var rightEye = new snowBall(38,63,1.8,"black");
    
    //2nd snowman on the far right
    var head_2 = new snowBall(width-40,60,15,"white");
    var sHead_2 = new snowBall(width-43,59,15,"lightgray");
    var sMiddle_2 = new snowBall(width-40,28,20,"lightgray");
    var middle_2 = new snowBall(width-43,30,20,"white");
    var sLow_2 = new snowBall(width-40,-5,25,"lightgray");
    var low_2 = new snowBall(width-43,0,25,"white");
    
    var leftEye_2 = new snowBall(width - 38,63,1.8,"black");
    var rightEye_2 = new snowBall(width - 48,63,1.8,"black");
    
    var snowBalls = [sLow_2,low_2,sMiddle_2,middle_2,sHead_2,head_2,sLow,low,sMiddle,middle,sHead,head,leftEye,
                     rightEye,rightEye_2,leftEye_2];
    
    
    
    var start = width;
    //the x stays the same for most
    
    //the loop for making the circles
   if(index_2 < snowBalls.length - 1){ //-1 for adding the shadows at the same time with the snow balls
        var ball = snowBalls[index_2];
        ctx.fillStyle = ball.color;
        ctx.beginPath();
        ctx.arc(start - ball.dx, height - ball.dy, ball.rad * size, 0, 2 * Math.PI); // drawing the shadows
        ctx.fill();
        index_2++; //get the white ball
        ball = snowBalls[index_2];
        ctx.fillStyle = ball.color;
        ctx.beginPath();
        ctx.arc(start - ball.dx, height - ball.dy, ball.rad * size, 0, 2 * Math.PI); // drwaing the balls
        ctx.fill();       
   }
    
    //after the balls are finished
    if(index_2 == snowBalls.length){
        ctx.beginPath();
        ctx.arc(start - 33, height - 57, 7, 0, Math.PI); // smile
        ctx.stroke();
        
        ctx.beginPath()
        ctx.arc(41, 60, 7, Math.PI, Math.PI * .25,true); //quarter smile
        ctx.stroke();
    }
    
    else if(index_2 == snowBalls.length + 1){
        ctx.strokeStyle ="orange"; //nose
        ctx.beginPath();
        ctx.moveTo(start - 33, height - 63);
        ctx.lineTo(start - 33,height - 57);
        ctx.stroke();
        
        //higher dx more right
        //higher dy more down
        ctx.fillStyle = "orange"; //carrot nose for #2
        ctx.beginPath();
        ctx.moveTo(42, 60);
        ctx.lineTo(42,53);
        ctx.lineTo(53,59);
        ctx.lineTo(53,59);
        ctx.fill();
        
        
    }
    
    index_2++;
}

//constructor for the balls so we can add the balls in 
//by intervals like the list
function snowBall(dx,dy,rad,color){
    this.dx = dx;
    this.dy = dy;
    this.rad = rad;
    this.color = color;
}



//adds the item per amount of ms
function loadList(){
    timer = setInterval(contactList,2000);
}


//It adds snow balls per amount of ms ! like the list but with
//balls because its cool

function loadSnowMan(){
    timer2 = setInterval(snowMan,750);
}


//Thank note

var line = 'Thank you for coming!!!!!';
var line_2 = "Looking forward to be working along side you!!!!";
var timer1;
var timer2;
var char = 0;
var char2 = 0;


function writer(){
    if(char < line.length + 1){  
        $('#textAnimation').text(line.substring(0,char));
        
        char++;
        
    }
    
    if(char2 < line_2.length + 1){
        $('#textAnimation2').text(line_2.substring(0,char2));
        char2++;
    }
}

//delay the note
function countDown(){
    timer1 = setTimeout(loadNote,7000);
}
//delay on letters
function loadNote(){
    timer2 = setInterval(writer, 150);
}

